﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleBehavior : MonoBehaviour
{
    public GameObject Capsule;
    private float MaxVal = 10;
    private float _sign = 1;
    private float x;
    void Update()
    {
        x += _sign*Time.deltaTime * 10;
        Capsule.transform.localScale = new Vector3(x, 1, 1);
        Capsule.transform.rotation = Quaternion.Euler(0, x, 0);
        if(MaxVal <= Capsule.transform.localScale.magnitude)
        {
            _sign *= -1;
        }
        
    }

}
